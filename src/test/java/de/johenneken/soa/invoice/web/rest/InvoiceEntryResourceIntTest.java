package de.johenneken.soa.invoice.web.rest;

import de.johenneken.soa.invoice.InvoiceServiceApp;

import de.johenneken.soa.invoice.domain.InvoiceEntry;
import de.johenneken.soa.invoice.repository.InvoiceEntryRepository;
import de.johenneken.soa.invoice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static de.johenneken.soa.invoice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InvoiceEntryResource REST controller.
 *
 * @see InvoiceEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InvoiceServiceApp.class)
public class InvoiceEntryResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    @Autowired
    private InvoiceEntryRepository invoiceEntryRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInvoiceEntryMockMvc;

    private InvoiceEntry invoiceEntry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InvoiceEntryResource invoiceEntryResource = new InvoiceEntryResource(invoiceEntryRepository);
        this.restInvoiceEntryMockMvc = MockMvcBuilders.standaloneSetup(invoiceEntryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvoiceEntry createEntity(EntityManager em) {
        InvoiceEntry invoiceEntry = new InvoiceEntry()
            .description(DEFAULT_DESCRIPTION)
            .amount(DEFAULT_AMOUNT)
            .price(DEFAULT_PRICE);
        return invoiceEntry;
    }

    @Before
    public void initTest() {
        invoiceEntry = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvoiceEntry() throws Exception {
        int databaseSizeBeforeCreate = invoiceEntryRepository.findAll().size();

        // Create the InvoiceEntry
        restInvoiceEntryMockMvc.perform(post("/api/invoice-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceEntry)))
            .andExpect(status().isCreated());

        // Validate the InvoiceEntry in the database
        List<InvoiceEntry> invoiceEntryList = invoiceEntryRepository.findAll();
        assertThat(invoiceEntryList).hasSize(databaseSizeBeforeCreate + 1);
        InvoiceEntry testInvoiceEntry = invoiceEntryList.get(invoiceEntryList.size() - 1);
        assertThat(testInvoiceEntry.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testInvoiceEntry.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testInvoiceEntry.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createInvoiceEntryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invoiceEntryRepository.findAll().size();

        // Create the InvoiceEntry with an existing ID
        invoiceEntry.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvoiceEntryMockMvc.perform(post("/api/invoice-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceEntry)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceEntry in the database
        List<InvoiceEntry> invoiceEntryList = invoiceEntryRepository.findAll();
        assertThat(invoiceEntryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllInvoiceEntries() throws Exception {
        // Initialize the database
        invoiceEntryRepository.saveAndFlush(invoiceEntry);

        // Get all the invoiceEntryList
        restInvoiceEntryMockMvc.perform(get("/api/invoice-entries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invoiceEntry.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getInvoiceEntry() throws Exception {
        // Initialize the database
        invoiceEntryRepository.saveAndFlush(invoiceEntry);

        // Get the invoiceEntry
        restInvoiceEntryMockMvc.perform(get("/api/invoice-entries/{id}", invoiceEntry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(invoiceEntry.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingInvoiceEntry() throws Exception {
        // Get the invoiceEntry
        restInvoiceEntryMockMvc.perform(get("/api/invoice-entries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvoiceEntry() throws Exception {
        // Initialize the database
        invoiceEntryRepository.saveAndFlush(invoiceEntry);

        int databaseSizeBeforeUpdate = invoiceEntryRepository.findAll().size();

        // Update the invoiceEntry
        InvoiceEntry updatedInvoiceEntry = invoiceEntryRepository.findById(invoiceEntry.getId()).get();
        // Disconnect from session so that the updates on updatedInvoiceEntry are not directly saved in db
        em.detach(updatedInvoiceEntry);
        updatedInvoiceEntry
            .description(UPDATED_DESCRIPTION)
            .amount(UPDATED_AMOUNT)
            .price(UPDATED_PRICE);

        restInvoiceEntryMockMvc.perform(put("/api/invoice-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInvoiceEntry)))
            .andExpect(status().isOk());

        // Validate the InvoiceEntry in the database
        List<InvoiceEntry> invoiceEntryList = invoiceEntryRepository.findAll();
        assertThat(invoiceEntryList).hasSize(databaseSizeBeforeUpdate);
        InvoiceEntry testInvoiceEntry = invoiceEntryList.get(invoiceEntryList.size() - 1);
        assertThat(testInvoiceEntry.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testInvoiceEntry.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testInvoiceEntry.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingInvoiceEntry() throws Exception {
        int databaseSizeBeforeUpdate = invoiceEntryRepository.findAll().size();

        // Create the InvoiceEntry

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvoiceEntryMockMvc.perform(put("/api/invoice-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceEntry)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceEntry in the database
        List<InvoiceEntry> invoiceEntryList = invoiceEntryRepository.findAll();
        assertThat(invoiceEntryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInvoiceEntry() throws Exception {
        // Initialize the database
        invoiceEntryRepository.saveAndFlush(invoiceEntry);

        int databaseSizeBeforeDelete = invoiceEntryRepository.findAll().size();

        // Get the invoiceEntry
        restInvoiceEntryMockMvc.perform(delete("/api/invoice-entries/{id}", invoiceEntry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InvoiceEntry> invoiceEntryList = invoiceEntryRepository.findAll();
        assertThat(invoiceEntryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceEntry.class);
        InvoiceEntry invoiceEntry1 = new InvoiceEntry();
        invoiceEntry1.setId(1L);
        InvoiceEntry invoiceEntry2 = new InvoiceEntry();
        invoiceEntry2.setId(invoiceEntry1.getId());
        assertThat(invoiceEntry1).isEqualTo(invoiceEntry2);
        invoiceEntry2.setId(2L);
        assertThat(invoiceEntry1).isNotEqualTo(invoiceEntry2);
        invoiceEntry1.setId(null);
        assertThat(invoiceEntry1).isNotEqualTo(invoiceEntry2);
    }
}
