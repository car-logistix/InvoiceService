package de.johenneken.soa.invoice.web.rest;

import de.johenneken.soa.invoice.InvoiceServiceApp;

import de.johenneken.soa.invoice.domain.InvoiceComment;
import de.johenneken.soa.invoice.repository.InvoiceCommentRepository;
import de.johenneken.soa.invoice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static de.johenneken.soa.invoice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InvoiceCommentResource REST controller.
 *
 * @see InvoiceCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InvoiceServiceApp.class)
public class InvoiceCommentResourceIntTest {

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    @Autowired
    private InvoiceCommentRepository invoiceCommentRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInvoiceCommentMockMvc;

    private InvoiceComment invoiceComment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InvoiceCommentResource invoiceCommentResource = new InvoiceCommentResource(invoiceCommentRepository);
        this.restInvoiceCommentMockMvc = MockMvcBuilders.standaloneSetup(invoiceCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvoiceComment createEntity(EntityManager em) {
        InvoiceComment invoiceComment = new InvoiceComment()
            .text(DEFAULT_TEXT)
            .user(DEFAULT_USER);
        return invoiceComment;
    }

    @Before
    public void initTest() {
        invoiceComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvoiceComment() throws Exception {
        int databaseSizeBeforeCreate = invoiceCommentRepository.findAll().size();

        // Create the InvoiceComment
        restInvoiceCommentMockMvc.perform(post("/api/invoice-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceComment)))
            .andExpect(status().isCreated());

        // Validate the InvoiceComment in the database
        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeCreate + 1);
        InvoiceComment testInvoiceComment = invoiceCommentList.get(invoiceCommentList.size() - 1);
        assertThat(testInvoiceComment.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testInvoiceComment.getUser()).isEqualTo(DEFAULT_USER);
    }

    @Test
    @Transactional
    public void createInvoiceCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invoiceCommentRepository.findAll().size();

        // Create the InvoiceComment with an existing ID
        invoiceComment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvoiceCommentMockMvc.perform(post("/api/invoice-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceComment)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceComment in the database
        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTextIsRequired() throws Exception {
        int databaseSizeBeforeTest = invoiceCommentRepository.findAll().size();
        // set the field null
        invoiceComment.setText(null);

        // Create the InvoiceComment, which fails.

        restInvoiceCommentMockMvc.perform(post("/api/invoice-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceComment)))
            .andExpect(status().isBadRequest());

        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = invoiceCommentRepository.findAll().size();
        // set the field null
        invoiceComment.setUser(null);

        // Create the InvoiceComment, which fails.

        restInvoiceCommentMockMvc.perform(post("/api/invoice-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceComment)))
            .andExpect(status().isBadRequest());

        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInvoiceComments() throws Exception {
        // Initialize the database
        invoiceCommentRepository.saveAndFlush(invoiceComment);

        // Get all the invoiceCommentList
        restInvoiceCommentMockMvc.perform(get("/api/invoice-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invoiceComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }
    
    @Test
    @Transactional
    public void getInvoiceComment() throws Exception {
        // Initialize the database
        invoiceCommentRepository.saveAndFlush(invoiceComment);

        // Get the invoiceComment
        restInvoiceCommentMockMvc.perform(get("/api/invoice-comments/{id}", invoiceComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(invoiceComment.getId().intValue()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInvoiceComment() throws Exception {
        // Get the invoiceComment
        restInvoiceCommentMockMvc.perform(get("/api/invoice-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvoiceComment() throws Exception {
        // Initialize the database
        invoiceCommentRepository.saveAndFlush(invoiceComment);

        int databaseSizeBeforeUpdate = invoiceCommentRepository.findAll().size();

        // Update the invoiceComment
        InvoiceComment updatedInvoiceComment = invoiceCommentRepository.findById(invoiceComment.getId()).get();
        // Disconnect from session so that the updates on updatedInvoiceComment are not directly saved in db
        em.detach(updatedInvoiceComment);
        updatedInvoiceComment
            .text(UPDATED_TEXT)
            .user(UPDATED_USER);

        restInvoiceCommentMockMvc.perform(put("/api/invoice-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInvoiceComment)))
            .andExpect(status().isOk());

        // Validate the InvoiceComment in the database
        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeUpdate);
        InvoiceComment testInvoiceComment = invoiceCommentList.get(invoiceCommentList.size() - 1);
        assertThat(testInvoiceComment.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testInvoiceComment.getUser()).isEqualTo(UPDATED_USER);
    }

    @Test
    @Transactional
    public void updateNonExistingInvoiceComment() throws Exception {
        int databaseSizeBeforeUpdate = invoiceCommentRepository.findAll().size();

        // Create the InvoiceComment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvoiceCommentMockMvc.perform(put("/api/invoice-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceComment)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceComment in the database
        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInvoiceComment() throws Exception {
        // Initialize the database
        invoiceCommentRepository.saveAndFlush(invoiceComment);

        int databaseSizeBeforeDelete = invoiceCommentRepository.findAll().size();

        // Get the invoiceComment
        restInvoiceCommentMockMvc.perform(delete("/api/invoice-comments/{id}", invoiceComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InvoiceComment> invoiceCommentList = invoiceCommentRepository.findAll();
        assertThat(invoiceCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceComment.class);
        InvoiceComment invoiceComment1 = new InvoiceComment();
        invoiceComment1.setId(1L);
        InvoiceComment invoiceComment2 = new InvoiceComment();
        invoiceComment2.setId(invoiceComment1.getId());
        assertThat(invoiceComment1).isEqualTo(invoiceComment2);
        invoiceComment2.setId(2L);
        assertThat(invoiceComment1).isNotEqualTo(invoiceComment2);
        invoiceComment1.setId(null);
        assertThat(invoiceComment1).isNotEqualTo(invoiceComment2);
    }
}
