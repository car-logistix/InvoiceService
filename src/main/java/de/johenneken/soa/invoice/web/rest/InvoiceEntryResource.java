package de.johenneken.soa.invoice.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.johenneken.soa.invoice.domain.InvoiceEntry;
import de.johenneken.soa.invoice.repository.InvoiceEntryRepository;
import de.johenneken.soa.invoice.web.rest.errors.BadRequestAlertException;
import de.johenneken.soa.invoice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InvoiceEntry.
 */
@RestController
@RequestMapping("/api")
public class InvoiceEntryResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceEntryResource.class);

    private static final String ENTITY_NAME = "invoiceServiceInvoiceEntry";

    private final InvoiceEntryRepository invoiceEntryRepository;

    public InvoiceEntryResource(InvoiceEntryRepository invoiceEntryRepository) {
        this.invoiceEntryRepository = invoiceEntryRepository;
    }

    /**
     * POST  /invoice-entries : Create a new invoiceEntry.
     *
     * @param invoiceEntry the invoiceEntry to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invoiceEntry, or with status 400 (Bad Request) if the invoiceEntry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invoice-entries")
    @Timed
    public ResponseEntity<InvoiceEntry> createInvoiceEntry(@RequestBody InvoiceEntry invoiceEntry) throws URISyntaxException {
        log.debug("REST request to save InvoiceEntry : {}", invoiceEntry);
        if (invoiceEntry.getId() != null) {
            throw new BadRequestAlertException("A new invoiceEntry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvoiceEntry result = invoiceEntryRepository.save(invoiceEntry);
        return ResponseEntity.created(new URI("/api/invoice-entries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invoice-entries : Updates an existing invoiceEntry.
     *
     * @param invoiceEntry the invoiceEntry to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated invoiceEntry,
     * or with status 400 (Bad Request) if the invoiceEntry is not valid,
     * or with status 500 (Internal Server Error) if the invoiceEntry couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invoice-entries")
    @Timed
    public ResponseEntity<InvoiceEntry> updateInvoiceEntry(@RequestBody InvoiceEntry invoiceEntry) throws URISyntaxException {
        log.debug("REST request to update InvoiceEntry : {}", invoiceEntry);
        if (invoiceEntry.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InvoiceEntry result = invoiceEntryRepository.save(invoiceEntry);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoiceEntry.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invoice-entries : get all the invoiceEntries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of invoiceEntries in body
     */
    @GetMapping("/invoice-entries")
    @Timed
    public List<InvoiceEntry> getAllInvoiceEntries() {
        log.debug("REST request to get all InvoiceEntries");
        return invoiceEntryRepository.findAll();
    }

    /**
     * GET  /invoice-entries/:id : get the "id" invoiceEntry.
     *
     * @param id the id of the invoiceEntry to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the invoiceEntry, or with status 404 (Not Found)
     */
    @GetMapping("/invoice-entries/{id}")
    @Timed
    public ResponseEntity<InvoiceEntry> getInvoiceEntry(@PathVariable Long id) {
        log.debug("REST request to get InvoiceEntry : {}", id);
        Optional<InvoiceEntry> invoiceEntry = invoiceEntryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(invoiceEntry);
    }

    /**
     * DELETE  /invoice-entries/:id : delete the "id" invoiceEntry.
     *
     * @param id the id of the invoiceEntry to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invoice-entries/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvoiceEntry(@PathVariable Long id) {
        log.debug("REST request to delete InvoiceEntry : {}", id);

        invoiceEntryRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
