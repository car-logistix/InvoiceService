/**
 * View Models used by Spring MVC REST controllers.
 */
package de.johenneken.soa.invoice.web.rest.vm;
