package de.johenneken.soa.invoice.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.johenneken.soa.invoice.domain.InvoiceComment;
import de.johenneken.soa.invoice.repository.InvoiceCommentRepository;
import de.johenneken.soa.invoice.web.rest.errors.BadRequestAlertException;
import de.johenneken.soa.invoice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InvoiceComment.
 */
@RestController
@RequestMapping("/api")
public class InvoiceCommentResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceCommentResource.class);

    private static final String ENTITY_NAME = "invoiceServiceInvoiceComment";

    private final InvoiceCommentRepository invoiceCommentRepository;

    public InvoiceCommentResource(InvoiceCommentRepository invoiceCommentRepository) {
        this.invoiceCommentRepository = invoiceCommentRepository;
    }

    /**
     * POST  /invoice-comments : Create a new invoiceComment.
     *
     * @param invoiceComment the invoiceComment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invoiceComment, or with status 400 (Bad Request) if the invoiceComment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invoice-comments")
    @Timed
    public ResponseEntity<InvoiceComment> createInvoiceComment(@Valid @RequestBody InvoiceComment invoiceComment) throws URISyntaxException {
        log.debug("REST request to save InvoiceComment : {}", invoiceComment);
        if (invoiceComment.getId() != null) {
            throw new BadRequestAlertException("A new invoiceComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvoiceComment result = invoiceCommentRepository.save(invoiceComment);
        return ResponseEntity.created(new URI("/api/invoice-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * DELETE  /invoice-comments/:id : delete the "id" invoiceComment.
     *
     * @param id the id of the invoiceComment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invoice-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvoiceComment(@PathVariable Long id) {
        log.debug("REST request to delete InvoiceComment : {}", id);

        invoiceCommentRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
