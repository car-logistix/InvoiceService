package de.johenneken.soa.invoice.repository;

import de.johenneken.soa.invoice.domain.InvoiceComment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InvoiceComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceCommentRepository extends JpaRepository<InvoiceComment, Long> {

}
