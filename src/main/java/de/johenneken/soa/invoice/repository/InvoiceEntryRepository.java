package de.johenneken.soa.invoice.repository;

import de.johenneken.soa.invoice.domain.InvoiceEntry;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InvoiceEntry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceEntryRepository extends JpaRepository<InvoiceEntry, Long> {

}
