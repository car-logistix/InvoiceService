package de.johenneken.soa.invoice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import de.johenneken.soa.invoice.domain.enumeration.InvoiceStatus;

/**
 * A Invoice.
 */
@Entity
@Table(name = "invoice")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "invoice_nummer", nullable = false)
    private String invoiceNummer;

    @Column(name = "date_issued")
    private LocalDate dateIssued;

    @NotNull
    @Column(name = "invoice_adress", nullable = false)
    private String invoiceAdress;

    @NotNull
    @Column(name = "issuer_adress", nullable = false)
    private String issuerAdress;

    @NotNull
    @Column(name = "issuer_firm", nullable = false)
    private String issuerFirm;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private InvoiceStatus status;

    @NotNull
    @Column(name = "receiver", nullable = false)
    private String receiver;

    @OneToMany(mappedBy = "invoice")
    private Set<InvoiceComment> comments = new HashSet<>();

    @OneToMany(mappedBy = "invoice")
    private Set<InvoiceEntry> entries = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceNummer() {
        return invoiceNummer;
    }

    public Invoice invoiceNummer(String invoiceNummer) {
        this.invoiceNummer = invoiceNummer;
        return this;
    }

    public void setInvoiceNummer(String invoiceNummer) {
        this.invoiceNummer = invoiceNummer;
    }

    public LocalDate getDateIssued() {
        return dateIssued;
    }

    public Invoice dateIssued(LocalDate dateIssued) {
        this.dateIssued = dateIssued;
        return this;
    }

    public void setDateIssued(LocalDate dateIssued) {
        this.dateIssued = dateIssued;
    }

    public String getInvoiceAdress() {
        return invoiceAdress;
    }

    public Invoice invoiceAdress(String invoiceAdress) {
        this.invoiceAdress = invoiceAdress;
        return this;
    }

    public void setInvoiceAdress(String invoiceAdress) {
        this.invoiceAdress = invoiceAdress;
    }

    public String getIssuerAdress() {
        return issuerAdress;
    }

    public Invoice issuerAdress(String issuerAdress) {
        this.issuerAdress = issuerAdress;
        return this;
    }

    public void setIssuerAdress(String issuerAdress) {
        this.issuerAdress = issuerAdress;
    }

    public String getIssuerFirm() {
        return issuerFirm;
    }

    public Invoice issuerFirm(String issuerFirm) {
        this.issuerFirm = issuerFirm;
        return this;
    }

    public void setIssuerFirm(String issuerFirm) {
        this.issuerFirm = issuerFirm;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public Invoice dueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public Invoice status(InvoiceStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public String getReceiver() {
        return receiver;
    }

    public Invoice receiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Set<InvoiceComment> getComments() {
        return comments;
    }

    public Invoice comments(Set<InvoiceComment> invoiceComments) {
        this.comments = invoiceComments;
        return this;
    }

    public Invoice addComments(InvoiceComment invoiceComment) {
        this.comments.add(invoiceComment);
        invoiceComment.setInvoice(this);
        return this;
    }

    public Invoice removeComments(InvoiceComment invoiceComment) {
        this.comments.remove(invoiceComment);
        invoiceComment.setInvoice(null);
        return this;
    }

    public void setComments(Set<InvoiceComment> invoiceComments) {
        this.comments = invoiceComments;
    }

    public Set<InvoiceEntry> getEntries() {
        return entries;
    }

    public Invoice entries(Set<InvoiceEntry> invoiceEntries) {
        this.entries = invoiceEntries;
        return this;
    }

    public Invoice addEntries(InvoiceEntry invoiceEntry) {
        this.entries.add(invoiceEntry);
        invoiceEntry.setInvoice(this);
        return this;
    }

    public Invoice removeEntries(InvoiceEntry invoiceEntry) {
        this.entries.remove(invoiceEntry);
        invoiceEntry.setInvoice(null);
        return this;
    }

    public void setEntries(Set<InvoiceEntry> invoiceEntries) {
        this.entries = invoiceEntries;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Invoice invoice = (Invoice) o;
        if (invoice.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoice.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Invoice{" +
            "id=" + getId() +
            ", invoiceNummer='" + getInvoiceNummer() + "'" +
            ", dateIssued='" + getDateIssued() + "'" +
            ", invoiceAdress='" + getInvoiceAdress() + "'" +
            ", issuerAdress='" + getIssuerAdress() + "'" +
            ", issuerFirm='" + getIssuerFirm() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", receiver='" + getReceiver() + "'" +
            "}";
    }
}
