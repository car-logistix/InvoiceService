package de.johenneken.soa.invoice.domain.enumeration;

/**
 * The InvoiceStatus enumeration.
 */
public enum InvoiceStatus {
    NEW, SENT, ACCEPTED, FINISHED
}
