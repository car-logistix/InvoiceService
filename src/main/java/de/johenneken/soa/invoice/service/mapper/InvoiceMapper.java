package de.johenneken.soa.invoice.service.mapper;

import de.johenneken.soa.invoice.domain.*;
import de.johenneken.soa.invoice.service.dto.InvoiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Invoice and its DTO InvoiceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InvoiceMapper extends EntityMapper<InvoiceDTO, Invoice> {

    @Mapping(target = "total",ignore = true)
    InvoiceDTO toDto(Invoice entity);

    default Invoice fromId(Long id) {
        if (id == null) {
            return null;
        }
        Invoice invoice = new Invoice();
        invoice.setId(id);
        return invoice;
    }
}
