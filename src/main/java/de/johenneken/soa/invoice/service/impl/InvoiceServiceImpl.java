package de.johenneken.soa.invoice.service.impl;

import de.johenneken.soa.invoice.domain.InvoiceEntry;
import de.johenneken.soa.invoice.domain.enumeration.InvoiceStatus;
import de.johenneken.soa.invoice.repository.InvoiceCommentRepository;
import de.johenneken.soa.invoice.repository.InvoiceEntryRepository;
import de.johenneken.soa.invoice.service.InvoiceService;
import de.johenneken.soa.invoice.domain.Invoice;
import de.johenneken.soa.invoice.repository.InvoiceRepository;
import de.johenneken.soa.invoice.service.dto.InvoiceDTO;
import de.johenneken.soa.invoice.service.mapper.InvoiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Invoice.
 */
@Service
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

    private final Logger log = LoggerFactory.getLogger(InvoiceServiceImpl.class);

    private final InvoiceRepository invoiceRepository;
    private final InvoiceEntryRepository invoiceEntryRepository;
    private final InvoiceCommentRepository invoiceCommentRepository;


    private final InvoiceMapper invoiceMapper;

    public InvoiceServiceImpl(InvoiceRepository invoiceRepository, InvoiceMapper invoiceMapper, InvoiceEntryRepository invoiceEntryRepository, InvoiceCommentRepository invoiceCommentRepository) {
        this.invoiceRepository = invoiceRepository;
        this.invoiceMapper = invoiceMapper;
        this.invoiceEntryRepository = invoiceEntryRepository;
        this.invoiceCommentRepository = invoiceCommentRepository;
    }

    /**
     * Save a invoice.
     *
     * @param invoiceDTO the entity to save
     * @param loginName
     * @return the persisted entity
     */
    @Override
    public InvoiceDTO save(InvoiceDTO invoiceDTO, String loginName) {
        log.debug("Request to save Invoice : {}", invoiceDTO);
        final Invoice invoice = invoiceMapper.toEntity(invoiceDTO);
        if(invoice.getId()==null){
            invoice.setStatus(InvoiceStatus.NEW);
        }
        invoice.setReceiver(loginName);
        invoice.getEntries().forEach(invoiceEntry -> invoiceEntry.setInvoice(invoice));
        invoiceEntryRepository.saveAll(invoice.getEntries());
        invoice.getComments().forEach(invoiceComment -> invoiceComment.setInvoice(invoice));
        invoiceCommentRepository.saveAll(invoice.getComments());
        Invoice invoiceDb = invoiceRepository.save(invoice);
        return calculateTotal(invoiceMapper.toDto(invoiceDb));
    }

    /**
     * Get all the invoices.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<InvoiceDTO> findAll() {
        log.debug("Request to get all Invoices");
        return invoiceRepository.findAll().stream()
            .map(invoiceMapper::toDto)
            .map(this::calculateTotal)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    InvoiceDTO calculateTotal(InvoiceDTO invoiceDTO) {
        Double total = 0.;
        for (InvoiceEntry invoiceEntry : invoiceDTO.getEntries()) {
            total += invoiceEntry.getAmount() * invoiceEntry.getPrice();
        }
        invoiceDTO.setTotal(total);
        return invoiceDTO;
    }


    /**
     * Get one invoice by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<InvoiceDTO> findOne(Long id) {
        log.debug("Request to get Invoice : {}", id);
        return invoiceRepository.findById(id)
            .map(invoiceMapper::toDto)
            .map(this::calculateTotal);
    }

    /**
     * Delete the invoice by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Invoice : {}", id);
        invoiceRepository.deleteById(id);
    }

    @Override
    public void send(Long id) {
        Invoice invoice = invoiceRepository.findById(id).get();
        invoice.setStatus(InvoiceStatus.SENT);
        invoiceRepository.save(invoice);
    }

    @Override
    public void finish(Long id) {
        Invoice invoice = invoiceRepository.findById(id).get();
        invoice.setStatus(InvoiceStatus.FINISHED);
        invoiceRepository.save(invoice);
    }

    @Override
    public void accept(Long id) {
        Invoice invoice = invoiceRepository.findById(id).get();
        invoice.setStatus(InvoiceStatus.ACCEPTED);
        invoiceRepository.save(invoice);
    }
}
