package de.johenneken.soa.invoice.service.dto;

import java.time.LocalDate;
import javax.persistence.OneToMany;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.johenneken.soa.invoice.domain.InvoiceComment;
import de.johenneken.soa.invoice.domain.InvoiceEntry;
import de.johenneken.soa.invoice.domain.enumeration.InvoiceStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.ReadOnlyProperty;

/**
 * A DTO for the Invoice entity.
 */
public class InvoiceDTO implements Serializable {

    private Long id;

    @NotNull
    private String invoiceNummer;

    private LocalDate dateIssued;

    @NotNull
    private String invoiceAdress;

    @NotNull
    private String issuerAdress;

    @NotNull
    private String issuerFirm;

    private LocalDate dueDate;

    @NotNull
    @ReadOnlyProperty
    private InvoiceStatus status;

    @NotNull
    @ReadOnlyProperty
    private String receiver;

    private Double total;

    @JsonIgnoreProperties("invoice")
    private Set<InvoiceComment> comments = new HashSet<>();

    @JsonIgnoreProperties("invoice")
    private Set<InvoiceEntry> entries = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceNummer() {
        return invoiceNummer;
    }

    public void setInvoiceNummer(String invoiceNummer) {
        this.invoiceNummer = invoiceNummer;
    }

    public LocalDate getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(LocalDate dateIssued) {
        this.dateIssued = dateIssued;
    }

    public String getInvoiceAdress() {
        return invoiceAdress;
    }

    public void setInvoiceAdress(String invoiceAdress) {
        this.invoiceAdress = invoiceAdress;
    }

    public String getIssuerAdress() {
        return issuerAdress;
    }

    public void setIssuerAdress(String issuerAdress) {
        this.issuerAdress = issuerAdress;
    }

    public String getIssuerFirm() {
        return issuerFirm;
    }

    public void setIssuerFirm(String issuerFirm) {
        this.issuerFirm = issuerFirm;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Set<InvoiceComment> getComments() {
        return comments;
    }

    public void setComments(Set<InvoiceComment> comments) {
        this.comments = comments;
    }

    public Set<InvoiceEntry> getEntries() {
        return entries;
    }

    public void setEntries(Set<InvoiceEntry> entries) {
        this.entries = entries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InvoiceDTO invoiceDTO = (InvoiceDTO) o;
        if (invoiceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
            "id=" + getId() +
            ", invoiceNummer='" + getInvoiceNummer() + "'" +
            ", dateIssued='" + getDateIssued() + "'" +
            ", invoiceAdress='" + getInvoiceAdress() + "'" +
            ", issuerAdress='" + getIssuerAdress() + "'" +
            ", issuerFirm='" + getIssuerFirm() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", receiver='" + getReceiver() + "'" +
            "}";
    }
}
